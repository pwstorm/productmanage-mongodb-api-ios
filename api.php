<?php

 // connect to host
$m = new MongoClient("mongodb://192.168.1.13:27017");

// select a database
$db = $m->officemate;

// select a collection
$collection = $db->product;

//get
$request = $_SERVER['PATH_INFO'];

//data from post request
$data = json_decode(file_get_contents('php://input'), true);

header('Content-Type: application/json');

$result = array();
$product = array();

//check url request and parameter
if (!empty($request) && !is_null($data)) {

   switch ($request) {
      case '/id':

         if ( (isset($data["id"])) && ($data["id"] != "") ) {
            //search data with parameter
            if ( $cursor = $collection->findOne(array('_id' => new MongoId($data["id"]))) ) {
               $id = $cursor['_id']->{'$id'};
               $result['product'] = array("id" => $id, "name" => $cursor["name"], "price" => $cursor["price"], "qty" => $cursor["qty"]);
            }else{
               $result['message'] = "not have result";
            }
         }else{
            $result['message'] = "not have product id request";
         }

         count($result) > 0 ? $result['isSuccess'] = true : $result['isSuccess'] = false;

         break;

      case '/create':
         //check parameter to insert
         if (isset($data['name']) && isset($data['price']) && isset($data['qty'])) {
            $document = array("name" => $data['name'], "price" => $data['price'], "qty" => $data['qty']);
            $collection->insert($document) ? $result['isSuccess'] = true : $result['isSuccess'] = false;
         }else{
            $result['isSuccess'] = false;
            $result['message'] = "not have parameter";
         }

         break;

      case '/update':
         //check parameter to update
         if (isset($data['id']) && isset($data['name']) && isset($data['price']) && isset($data['qty'])) {
            //update document with all parameter
            $document = array("name" => $data['name'], "price" => $data['price'], "qty" => $data['qty']);
            $newdata = array('$set' => $document);
            $collection->update(array('_id' => new MongoId($data['id'])), $newdata) ? $result['isSuccess'] = true : $result['isSuccess'] = false;
         }else{
            $result['isSuccess'] = false;
            $result['message'] = "not have parameter";
         }

         break;

      case '/delete':
         //check parameter to delete
         if (isset($data['id'])) {
            //remove document with id
            $collection->remove(array('_id' => new MongoId($data['id'])), array('justOne' => true)) ? $result['isSuccess'] = true : $result['isSuccess'] = false;
         }else{
            $result['isSuccess'] = false;
            $result['message'] = "not have parameter id";
         }

         break;
      
      default:
         
         $result['isSuccess'] = false;
         $result['message'] = "not have url request";

         break;
   }

}else{

   //check url request
   if ($request == '/find') {
   
      //find document with not have parameter
      $cursor = $collection->find();

      //fetch document from database
      foreach($cursor as $c) {
         $id = $c['_id']->{'$id'};
         $temp = array("id" => $id, "name" => $c["name"], "price" => $c["price"], "qty" => $c["qty"]);
         array_push($product, $temp);
      }

      $result['product'] = $product;

      if (count($product) == 0) {
         $result['message'] = "not have data";
      }

      count($result) > 0 ? $result['isSuccess'] = true : $result['isSuccess'] = false;

   }

}

//reponse result
//echo json_encode($result, JSON_PRETTY_PRINT);
echo json_encode($result);
