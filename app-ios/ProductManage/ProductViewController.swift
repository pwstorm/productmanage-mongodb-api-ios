//
//  ProductViewController.swift
//  ProductManage
//
//  Created by Patomphong Wongkalasin on 12/9/2559 BE.
//  Copyright © 2559 COL. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ProductViewController: UIViewController, UITextFieldDelegate {
    
    var id: String = ""
    
    @IBOutlet weak var textfieldProductName: UITextField!
    @IBOutlet weak var textfieldProductPrice: UITextField!
    @IBOutlet weak var textfieldProductQty: UITextField!
    @IBOutlet weak var buttonDone: UIBarButtonItem!
    
    let url : String = "http://192.168.1.13/officemate/mongodb/api.php/"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("id = \(id)")
        
        if id != "" {
            fetchProductWithID(id: id)
            
            print("title = \(self.navigationItem.title)")
            
            if self.navigationItem.title == "Product Detail" {
                print("view product detail")
                buttonDone.isEnabled = false
                textfieldProductName.isEnabled = false
                textfieldProductPrice.isEnabled = false
                textfieldProductQty.isEnabled = false
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchProductWithID(id: String) {
        
        let parameters: Parameters = [
            "id":id
        ]
        
        Alamofire.request(url + "id", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
            
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                print("JSON: \(json)")
                
                guard let isSuccess = json["isSuccess"].bool else {
                    return
                }
                
                if isSuccess {
                    print("fetch success")
                    
                    guard let name = json["product"]["name"].string else {
                        return
                    }
                    
                    guard let price = json["product"]["price"].double else {
                        return
                    }
                    
                    guard let qty = json["product"]["qty"].int else {
                        return
                    }
                    
                    self.textfieldProductName.text = name
                    self.textfieldProductPrice.text = "\(price)"
                    self.textfieldProductQty.text = "\(qty)"

                }else{
                    print("fetch fail")
                }
                
            case .failure(let error):
                print(error)
            }
        }
    }

    
    @IBAction func doneEditProduct() {
        print("press done button")
        
        guard let name = textfieldProductName.text else {
            return
        }
        
        guard let price = textfieldProductPrice.text else {
            return
        }
        
        guard let qty = textfieldProductQty.text else {
            return
        }
        
        if id == "" {
            //insert data

            if name != "" && price != "" && qty != "" {
                saveProduct(id: id, name: name, price: Double(price)!, qty: Int(qty)!)
            }else{
                print("cannot save product not have parameter")
                alert(title: "Failure", message: "Data Not Be Blank.\nPlease Fill Data In All Form.", status: false)
            }
        }else{
            //update data
            if name != "" && price != "" && qty != "" {
                saveProduct(id: id, name: name, price: Double(price)!, qty: Int(qty)!)
            }else{
                print("cannot save product not have parameter")
                alert(title: "Failure", message: "Data Not Be Blank.\nPlease Fill Data In All Form.", status: false)
            }
        }
        

        
    }
    
    func saveProduct(id: String, name: String, price: Double, qty: Int) {
        print("save product")
        
        
        var request = "create"
        
        if id != "" {
            request = "update"
        }

        let parameters: Parameters = [
            "id": id,
            "name": name,
            "price": price,
            "qty": qty
        ]
        
        Alamofire.request(url + request, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
            
            if let data = response.data {
                
                let json = JSON(data: data)
                
                print("JSON: \(json)")
                
                //                guard let isSuccess = json["isSuccess"].bool else {
                //                    return
                //                }
                
                if let isSuccess = json["isSuccess"].bool {
                    print("add product success")
                    
                    
                }else{
                    print("add product fail")
                    
                    guard let message = json["message"].bool else {
                        return
                    }
                    
                    print("message : \(message)")
                }
                
            }else{
                print("add product fail : no data response")
            }
        }

        
        alert(title: "Success", message: "Data Saved To Database Successfully", status: true)
    }
    
    func hideKeyboard() {
        print("hide keyboard")
        textfieldProductName.resignFirstResponder()
        textfieldProductPrice.resignFirstResponder()
        textfieldProductQty.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        hideKeyboard()
        return true
    }
    
    func alert(title: String, message: String, status: Bool) {
        hideKeyboard()
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let closeAction = UIAlertAction(title: "Close", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            print("Close")
            
            if status {
                self.performSegue(withIdentifier: "unwindToProdcutListVC", sender: self)
            }
        }
        alertController.addAction(closeAction)
        self.present(alertController, animated: true, completion: nil)
        

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
