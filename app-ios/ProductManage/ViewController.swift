//
//  ViewController.swift
//  ProductManage
//
//  Created by Patomphong Wongkalasin on 12/9/2559 BE.
//  Copyright © 2559 COL. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var id: String = ""
    
    var productID : [String] = []
    var productName : [String] = []
    var productPrice : [Double] = []
    var productQty : [Int] = []
    
    let url : String = "http://192.168.1.13/officemate/mongodb/api.php/"

    override func viewWillAppear(_ animated: Bool) {
        id = ""
        fetchProduct()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Product List"
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let viewHeader = UIView()
        viewHeader.frame = CGRect(x:0, y:0, width:UIScreen.main.bounds.width, height:30)
        viewHeader.backgroundColor = .gray
        
        let labelHeaderName = UILabel()
        let labelHeaderPrice = UILabel()
        let labelHeaderQty = UILabel()
        
        labelHeaderName.text="Name"
        labelHeaderName.textColor = .white
        labelHeaderPrice.text="In Stock"
        labelHeaderPrice.textColor = .white
        labelHeaderQty.text="Price"
        labelHeaderQty.textColor = .white
        
        viewHeader.addSubview(labelHeaderName)
        viewHeader.addSubview(labelHeaderPrice)
        viewHeader.addSubview(labelHeaderQty)
        
        labelHeaderName.translatesAutoresizingMaskIntoConstraints = false
        labelHeaderPrice.translatesAutoresizingMaskIntoConstraints = false
        labelHeaderQty.translatesAutoresizingMaskIntoConstraints = false
        
        let views = ["labelHeaderName": labelHeaderName, "labelHeaderPrice": labelHeaderPrice, "labelHeaderQty": labelHeaderQty, "view": viewHeader]
        
        let horizontallayoutContraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-20-[labelHeaderName]-20-[labelHeaderPrice]-35-[labelHeaderQty]-50-|", options: .alignAllCenterY, metrics: nil, views: views)
        viewHeader.addConstraints(horizontallayoutContraints)
        
        let verticalLayoutContraint = NSLayoutConstraint(item: labelHeaderName, attribute: .centerY, relatedBy: .equal, toItem: viewHeader, attribute: .centerY, multiplier: 1, constant: 0)
        let verticalLayoutContraint2 = NSLayoutConstraint(item: labelHeaderPrice, attribute: .centerY, relatedBy: .equal, toItem: viewHeader, attribute: .centerY, multiplier: 1, constant: 0)
        let verticalLayoutContraint3 = NSLayoutConstraint(item: labelHeaderQty, attribute: .centerY, relatedBy: .equal, toItem: viewHeader, attribute: .centerY, multiplier: 1, constant: 0)
        viewHeader.addConstraint(verticalLayoutContraint)
        viewHeader.addConstraint(verticalLayoutContraint2)
        viewHeader.addConstraint(verticalLayoutContraint3)
        
        return viewHeader
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productID.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ProductListTableViewCell
        
        cell.labelName.text = productName[indexPath.row]
        cell.labelQty.text = "\(productQty[indexPath.row])"
        cell.labelPrice.text = "\(productPrice[indexPath.row])฿"
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected row \(indexPath.row)")
        self.id = self.productID[indexPath.row]
        
        print("selected id \(self.id)")
        
        self.performSegue(withIdentifier: "viewProduct", sender: self)
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .default, title: "Delete") {action, index in
            //handle delete
            print("delete row \(indexPath.row)")
            
            self.deleteProduct(id: self.productID[indexPath.row])
            
            tableView.reloadData()

        }
        
        deleteAction.backgroundColor = UIColor.red
        
        let editAction = UITableViewRowAction(style: .normal, title: " Edit    ") {action, index in
            //handle edit
            print("edit row \(indexPath.row)")
            
            self.id = self.productID[indexPath.row]
            
            self.editProduct(id: self.productID[indexPath.row], name: self.productName[indexPath.row], price: self.productPrice[indexPath.row], qty: self.productQty[indexPath.row])
            
            self.performSegue(withIdentifier: "editProduct", sender: self)
            
        }
        
        editAction.backgroundColor = UIColor.gray
        
        
        return [deleteAction, editAction]
    }
    
    func fetchProduct() {
        
        print("fetch product")
        
        self.resetData()
        
        Alamofire.request(url + "find").responseJSON { response in
            
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                print("JSON: \(json)")
                
                guard let isSuccess = json["isSuccess"].bool else {
                    return
                }
                
                if isSuccess {
                    print("fetch success")
                    for (_, product) in json["product"] {
                        self.productID.append(product["id"].string!)
                        self.productName.append(product["name"].string!)
                        self.productPrice.append(product["price"].double!)
                        self.productQty.append(product["qty"].int!)
                    }
                    
                    self.tableView.reloadData()
                    
                }else{
                    print("fetch fail")
                }
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    @IBAction func addProduct() {
        print("press add product button")
    }
    
    func editProduct(id: String, name: String, price: Double, qty: Int) {
        print("edit product")
    }
    
    func deleteProduct(id: String) {
        
        print("id = \(id)")
        
        let parameters: Parameters = [
            "id": id
        ]
        
        Alamofire.request("http://192.168.1.13/officemate/mongodb/api.php/delete", method: .post, parameters: parameters, encoding: JSONEncoding(options: [])).response { response in
            
            if let data = response.data {
                
                let json = JSON(data: data)

                print("JSON: \(json)")
                
                if let isSuccess = json["isSuccess"].bool {
                    print("delete success")
                    
                    self.alert(title: "Success", message: "Deleted Product Successfully.")
                    
                    self.fetchProduct()
                    
                }else{
                    print("delete fail")
                    
                    guard let message = json["message"].string else {
                        return
                    }
                    
                    self.alert(title: "Failure", message: message)
                }
                
            }else{
                self.alert(title: "Failure", message: "No Data Response")
            }
        }
        
    }
    
    func resetData() {
        productID = []
        productName = []
        productPrice = []
        productQty = []
    }
    
    func alert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let closeAction = UIAlertAction(title: "Close", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            print("Close")
        }
        alertController.addAction(closeAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func unwindToProdcutListVC(_ segue: UIStoryboardSegue) {
        print("unwind to product list view controller")
        id = ""
        
    }

     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
        
        let destinationVC = segue.destination as! ProductViewController
    
        switch segue.identifier! {
        case "addProduct":
            print("go to add product")
            destinationVC.navigationItem.title = "Add Product"
        case "editProduct":
            print("go to edit product")
            destinationVC.navigationItem.title = "Edit Product"
            destinationVC.id = self.id
        case "viewProduct":
            print("go to view product")
            
            if id != "" {
                print("product id = " + self.id)
                destinationVC.navigationItem.title = "Product Detail"
                destinationVC.id = self.id
            }else{
                print("not have id")
            }
            
            
        default:
            print("error : not have segue identifier")
        }
        
     }


}

